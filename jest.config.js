// [v0.1.001-20240802]

const cmdArgs = require('minimist')(process.argv.slice(2));

const testObj = new Map([
  [ 'xml:origin', 'origin' ],
  //[ 'bsc:colns', 'baseClass/TNamedSetsCollection' ],
]);

let rootDirExt = '';
let obj = cmdArgs.object;
let tmSym = cmdArgs.target;
if (
  typeof tmSym !== 'string'
  || ((tmSym = tmSym.trim()) === '')
) {
  tmSym = '*';
};
if (
  typeof obj === 'string'
  && ((obj = obj.trim()) !== '')
) {
  if (testObj.has(obj)) rootDirExt = `/${testObj.get(obj)}/`;
};

/* used to run tests from origin */
const rootDir = require('path').resolve(__dirname);
//console.log(`TEST [*]: ${rootDir}`);
//const setupFilesAfterEnv = ['#test-dir/origin/pretest-jest.js'];
const modulePaths = [
  `${rootDir}/lib`,
  `${rootDir}/node_modules`,
];
/* ---//--- */

module.exports = {
  rootDir: `__test__${rootDirExt}`,
  testEnvironment: 'node',
  testMatch: [`**/?(${tmSym}.)+(spec|test).[jt]s?(x)`],
  //setupFilesAfterEnv,
  modulePaths,
  //verbose: false,
};
