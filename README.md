|***rev.*:**|0.0.3|
|:---|---:|
|***date***:|2024-08-02|

## Introduction

Yep,.. there's yet another fork of 'xml-js' package written by Nashwaan for node.js. Also it was adapted for my needs.

### Main differences against origin

For now, there is:

	- it ships with a SAX-parse onboard;
	- support for cli was dropped.

---

## Licenses

>All original licenses for used components (see the table bellow) and/or its parts shipped with this package within its '`LICENSES.d`' directory.
>
>|module|version|license|
>|:---|---:|---:|
>|[xml-js](https://www.npmjs.com/package/xml-js)|1.6.11|MIT|
>|[sax](https://www.npmjs.com/package/sax)|1.4.1|ISC|
