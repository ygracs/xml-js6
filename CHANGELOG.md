#### *v0.0.4b*

Pre-release version.

> update 'sax.js' module to v1.4.1 (from origin).

#### *v0.0.3b*

Pre-release version.

> - linked to GitLab-repository.

#### *v0.0.3*

Pre-release version.

> - MIT-license applied.

#### *v0.0.2*

Pre-release version.

> - `$module/lib/xml-js/array-helper.js` deprecated.

#### *v0.0.1*

Pre-release version.

> - merging original resources from 'sax' and 'xml-js' in a single module.
