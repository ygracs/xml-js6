/*jslint node:true */

var xml2js = require('#lib/xml-js/xml2js.js');
var xml2json = require('#lib/xml-js/xml2json.js');
var js2xml = require('#lib/xml-js/js2xml.js');
var json2xml = require('#lib/xml-js/json2xml.js');

module.exports = {
  xml2js: xml2js,
  xml2json: xml2json,
  js2xml: js2xml,
  json2xml: json2xml
};
