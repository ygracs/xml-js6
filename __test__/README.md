>|***rev.*:**|0.0.1|
>|:---|---:|
>|date:|2023-08-02|

## Introduction

This paper describes a tests shipped within the package.

## Provided tests

- `origin` test group

  (*this is original tests shipped within original package*)

## Use cases

### All tests

To run all tests available use the next command:

`npm test`

or

`npm run jest`

### Separate module tests

To run separate tests for each module or class you can use `--object` option:

|option value|module or class name|description|
|---|---|---|
|`xml:origin`|`xml-js`|run tests for `origin` test group|

> Example: `npm test -- --object="xml:origin"`
